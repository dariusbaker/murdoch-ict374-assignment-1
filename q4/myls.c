#include <errno.h>
#include <dirent.h>
#include <grp.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sysmacros.h> // for major() and minor()
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>

#define DATETIME_BUF_SIZE 16
#define DATETIME_FORMAT   "%b %e %R"

/**
 * Function declarations.
 */
void   list_dir(char dirname[]);
void   print_file_info(char * file_name, struct stat * file_status);
void   get_permissions(int st_mode, char* str);
void   get_time(char * str, time_t datetime);
void   run_stat(char * file_name);
char * get_file_type(int st_mode);
char * get_user_name(uid_t user_id);
char * get_group_name(gid_t group_id);

/**
 * Runs 'stat' on specified file.
 * @param {char *} file_name
 */
void run_stat(char * file_name) {
	struct stat file_status;

	// print file information if stat runs successfully
	if (stat(file_name, &file_status) >= 0) {
		print_file_info(file_name, &file_status);
	} else {
		fprintf(stderr, "Error stat-ing on '%s': %s.\n", file_name, strerror(errno));
	}

	return;
}

/**
 * Opens and reads specified directory, then prints information on entries.
 * @param {char[]} dirname
 */
void list_dir(char dirname[]) {
	DIR * dir = opendir(dirname);
	struct dirent * entry;

	// exit if we can't open the directory, else get files and print info
	if (dir == NULL) {
		fprintf(stderr, "Error opening '%s': %s.\n", dirname, strerror(errno));
		exit(1);
	} else {
		// loop through directory entries
		while ((entry = readdir(dir)) != NULL) {
			// we'll skip two directories, '.' and '..', since we aren't asked to emulate ls -a
			if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
				run_stat(entry->d_name);
			}
		}
	}

	return;
}

/**
 * Prints file information.
 * Assumes that struct stat has already been successfully read.
 * @param {char *}        file_name
 * @param {struct stat *} file_status
 */
void print_file_info(char * file_name, struct stat * file_status) {
	char * file_type;
	char * user_name;
	char * group_name;
	char   device_number[32];
	char   accessed_time[DATETIME_BUF_SIZE];
	char   modified_time[DATETIME_BUF_SIZE];
	char   status_time[DATETIME_BUF_SIZE];
	char   permissions[11] = "----------\0";

	file_type  = get_file_type(file_status->st_mode);
	user_name  = get_user_name(file_status->st_uid);
	group_name = get_group_name(file_status->st_gid);

	// get permissions in desired format
	get_permissions(file_status->st_mode, permissions);

	// get date and time in desired format
	get_time(accessed_time, file_status->st_atime);
	get_time(modified_time, file_status->st_mtime);
	get_time(status_time, file_status->st_ctime);

	// combine device number with major and minor numbers
	sprintf(
		device_number,
		"%ld (major: %d, minor: %d)",
		file_status->st_dev,
		major(file_status->st_dev),
		minor(file_status->st_dev)
	);

	printf("-----------------------------------------------------------------------\n");
	printf("| %-67s |\n", file_name);
	printf("|---------------------------------------------------------------------|\n");
	printf("| %-32s | %-32s |\n",  "User name (owner owner)",  user_name);
	printf("| %-32s | %-32s |\n",  "Group name (group owner)", group_name);
	printf("| %-32s | %-32s |\n",  "Type",                     file_type);
	printf("| %-32s | %-32s |\n",  "Access permissions",       permissions);
	printf("| %-32s | %-32ld |\n", "Size",                     file_status->st_size);
	printf("| %-32s | %-32ld |\n", "Index node number",        file_status->st_ino);
	printf("| %-32s | %-32s |\n",  "Device number",            device_number);
	printf("| %-32s | %-32ld |\n", "Links",                    file_status->st_nlink);
	printf("| %-32s | %-32s |\n",  "Last accessed",            accessed_time);
	printf("| %-32s | %-32s |\n",  "Last modified",            modified_time);
	printf("| %-32s | %-32s |\n",  "Last status change",       status_time);
	printf("-----------------------------------------------------------------------\n");

	return;
}

/**
 * Returns date and time formatted in MMM D HH:MM. e.g. 'Nov  1 11:31' or 'Jan 13 01:10'
 * @param {char *} str
 * @param {time_t} datetime
 */
void get_time(char * str, time_t datetime) {
	strftime(str, DATETIME_BUF_SIZE, DATETIME_FORMAT, localtime(&datetime));
}

/**
 * Returns string describing file type.
 * @param  {int} st_mode
 * @return {char *}
 */
char * get_file_type(int st_mode) {
	char * type = "Unknown";

	if (S_ISREG(st_mode)) {
		type = "File";
	} else if (S_ISDIR(st_mode)) {
		type = "Directory";
	} else if (S_ISCHR(st_mode)) {
		type = "Character special";
	} else if (S_ISBLK(st_mode)) {
		type = "Block special";
	} else if (S_ISFIFO(st_mode)) {
		type = "FIFO";
	} else if (S_ISLNK(st_mode)) {
		type = "Symbolic link";
	} else if (S_ISSOCK(st_mode)) {
		type = "Socket";
	}

	return type;
}

/**
 * Gets file permissions in -rwxrwxrxw format.
 * @param {int}    st_mode
 * @param {char *} permissions
 */
void get_permissions(int st_mode, char * permissions) {
	// determine file type
	if (st_mode & S_ISDIR(st_mode)) {
		permissions[0] = 'd';
	} else if (st_mode & S_ISCHR(st_mode)) {
		permissions[0] = 'c';
	} else if (st_mode & S_ISBLK(st_mode)) {
		permissions[0] = 'b';
	}

	// determine user owner permissions
	if (st_mode & S_IRUSR) {
		permissions[1] = 'r';
	}
	if (st_mode & S_IWUSR) {
		permissions[2] = 'w';
	}
	if (st_mode & S_IXUSR) {
		permissions[3] = 'x';
	}

	// determine group owner permissions
	if (st_mode & S_IRGRP) {
		permissions[4] = 'r';
	}
	if (st_mode & S_IWGRP) {
		permissions[5] = 'w';
	}
	if (st_mode & S_IXGRP) {
		permissions[6] = 'x';
	}

	// determine permissions for other users
	if (st_mode & S_IROTH) {
		permissions[7] = 'r';
	}
	if (st_mode & S_IWOTH) {
		permissions[8] = 'w';
	}
	if (st_mode & S_IXOTH) {
		permissions[9] = 'x';
	}

	return;
}

/**
 * Returns name of owner.
 * @param  {uid_t} user_id
 * @return {char *}
 */
char * get_user_name(uid_t user_id) {
	struct passwd * pw;
	char * user_name;

	pw = getpwuid(user_id);

	if (pw == NULL) {
		printf("Error getting user name of %d.\n", user_id);
		exit(1);
	} else {
		user_name = pw->pw_name;
	}

	return user_name;
}

/**
 * Returns name of group owner.
 * @param  {gid_t} group_id
 * @return {char *}
 */
char * get_group_name(gid_t group_id) {
	struct group * grp;
	char * group_name;

	grp = getgrgid(group_id);

	if (grp == NULL) {
		printf("Error getting group name of %d.\n", group_id);
		exit(1);
	} else {
		group_name = grp->gr_name;
	}

	return group_name;
}

/**
 * Driver program.
 * @param  {int}    argc
 * @param  {char *} argv
 * @return {int}
 */
int main(int argc, char * argv[]) {
	int i = 0;

	// print info on all files in current directory if no arguments are provided.
	if (argc == 1) {
		list_dir(".");
	} else {
		while (argv[++i] != NULL) {
			run_stat(argv[i]);
		}
	}

	exit(0);
}
