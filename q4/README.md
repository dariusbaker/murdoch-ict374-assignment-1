## Brief/ask

Write a C program, myls.c, that is similar to the standard Unix utility `ls -l` (but with much less functionality). Specifically, it takes a list of command line arguments, treating each command line argument as a file name. It then reports the following information for each file:

1. user name of the owner owner (hints: Stevens & Rago, 6.2.);
2. group name of the group owner; (hints: Stevens & Rago, 6.4.);
3. the type of file;
4. full access permissions, reported in the format used by the ls program;
5. the size of the file;
6. i-node number;
7. the device number of the device in which the file is stored, including both major number and minor number (hints: Stevens & Rago, 4.23.);
8. the number of links;
9. last access time, converted to the format used by the ls program (hint: Stevens & Rago, 6.10.);
10. last modification time, converted to the format used by the ls program;
11. last time file status changed, converted to the format used by the ls program;
Like `ls -l` command, if no command line argument is provided, the program simply reports the information about the files in the current directory.

Of course, your program cannot use `ls` program in any way.