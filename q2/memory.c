#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

typedef struct var {
	char   name[32]; // e.g. "global_pointer2"
	char   type[32]; // e.g. "char * []"
	char   inited;   // 'Y' or 'N'
	size_t size;     // size of variable in bytes
	size_t address;  // memory address
} Var;

extern char ** environ;

int    global_x = 10;                     // init-ed global var
int    global_y;                          // uninit-ed global var
char   global_array1[] = "Hello, world!"; // init-ed global array
char   global_array2[10];                 // uninit-ed global array
char * global_pointer1 = "bye!";          // init-ed global pointer to string literal
char * global_pointer2;                   // uninit-ed global pointer
float  global_float = 100.1;              // init-ed global var
double global_double;                     // uninit-ed global var

Var *  variables[50];
int    var_index = 0;

/**
 * Sort addresses in descending order.
 */
void sort_addresses() {
	int i, j;
	Var * temp_var = malloc(sizeof(Var));

	for (i = 0; i < var_index; i++) {
		for (j = i + 1; j < var_index; j++) {
			if (variables[i]->address < variables[j]->address) {
				// store values in temporary object
				temp_var->inited  = variables[i]->inited;
				temp_var->address = variables[i]->address;
				temp_var->size    = variables[i]->size;
				strcpy(temp_var->type, variables[i]->type);
				strcpy(temp_var->name, variables[i]->name);

				// swap values of i with j
				variables[i]->inited  = variables[j]->inited;
				variables[i]->address = variables[j]->address;
				variables[i]->size    = variables[j]->size;
				strcpy(variables[i]->name, variables[j]->name);
				strcpy(variables[i]->type, variables[j]->type);

				// replace values of j with temporarily stored object
				variables[j]->address = temp_var->address;
				variables[j]->inited  = temp_var->inited;
				variables[j]->size    = temp_var->size;
				strcpy(variables[j]->name, temp_var->name);
				strcpy(variables[j]->type, temp_var->type);
			}
		}
	}

	free(temp_var);

	return;
}

/**
 * Prints a table containing variable names, types, initialisation, and addresses.
 * Columns: index, name, type, initialised?, address in long int, address in hex.
 */
void print_addresses() {
	int i;
	int table_width = 117;

	for (i = 0; i < table_width; i++) printf("-");
	printf("\n");

	for (i = 0; i < var_index; i++) {
		printf(
			"| %-2d | %-24s | %-24s |  %c  | %-2lu | %-20zu | %-20zx |\n",
			i + 1,
			variables[i]->name,
			variables[i]->type,
			variables[i]->inited,
			variables[i]->size,
			variables[i]->address,
			variables[i]->address
		);
	}

	for (i = 0; i < table_width; i++) printf("-");
	printf("\n");

	return;
}

/**
 * Prints variables in CSV format.
 */
void print_csv() {
	int i;

	for (i = 0; i < var_index; i++) {
		if (i > 0) {
			printf("\n");
		}

		printf(
			"%s,%s,%c,%lu,%zu,%zx",
			variables[i]->name,
			variables[i]->type,
			variables[i]->inited,
			variables[i]->size,
			variables[i]->address,
			variables[i]->address
		);
	}

	printf("\n");

	return;
}

/**
 * Stores variable name and address in a struct.
 * @param {char *} name
 * @param {char *} type
 * @param {char}   inited
 * @param {size_t} size
 * @param {size_t} address
 */
void record(char * name, char * type, char inited, size_t size, size_t address) {
	// allocation
	variables[var_index] = malloc(sizeof(Var));

	// store values
	strcpy(variables[var_index]->type, type);
	strcpy(variables[var_index]->name, name);
	variables[var_index]->inited  = inited;
	variables[var_index]->address = address;
	variables[var_index]->size    = size;

	// increase count
	var_index++;

	return;
}

/**
 * @param  {int} x
 * @return {int}
 */
int f2(int x) {
	char * f2_p;      // uninit-ed local pointer
	int    f2_x = 21; // init-ed local var

	// dynamically allocated memory
	f2_p = malloc(1000);

	// function argument
	record("x", "func arg int", '-', sizeof(x), (size_t) &x);

	// local variables
	record("f2_p", "local pointer", 'N', sizeof(f2_p), (size_t) &f2_p);
	record("f2_x", "local int",     'Y', sizeof(f2_x), (size_t) &f2_x);

	// dynamically allocated memory
	record("f2_p start", "malloc", '-', sizeof(f2_p[0]), (size_t) &f2_p[0]); // &f2_p[0] and f2_p are the same

	f2_x = 10;

	return f2_x;
}

/**
 * @param {int}    x1
 * @param {int}    x2
 * @param {float}  x3
 * @param {char}   x4
 * @param {double} x5
 * @param {int}    x6
 */
void f1(int x1, int x2, float x3, char x4, double x5, int x6) {
	int    f1_x = 10;                     // init-ed local var
	int    f1_y;                          // uninit-ed local var
	char * f1_p1 = "This is inside f1()"; // init-ed local pointer to string literal
	char * f1_p2;                         // uninit-ed local pointer

	// dynamically allocated memory
	f1_p2 = malloc(100);

	// function arguments
	record("x1", "func arg int",    '-', sizeof(x1), (size_t) &x1);
	record("x2", "func arg int",    '-', sizeof(x2), (size_t) &x2);
	record("x3", "func arg float",  '-', sizeof(x3), (size_t) &x3);
	record("x4", "func arg char",   '-', sizeof(x4), (size_t) &x4);
	record("x5", "func arg double", '-', sizeof(x5), (size_t) &x5);
	record("x6", "func arg int",    '-', sizeof(x6), (size_t) &x6);

	// local variables
	record("f1_x",  "local int",    'Y', sizeof(f1_x),  (size_t) &f1_x);
	record("f1_y",  "local int",    'N', sizeof(f1_y),  (size_t) &f1_y);
	record("f1_p1", "local char *", 'Y', sizeof(f1_p1), (size_t) &f1_p1);
	record("f1_p2", "local char *", 'N', sizeof(f1_p2), (size_t) &f1_p2);

	// string literal
	record(
		"\"This is inside f1()\"",
		"local string literal",
		'-',
		sizeof(f1_p1),
		(size_t) f1_p1
	);

	// dynamically allocated memory
	record("f1_p2 start", "malloc", '-', sizeof(f1_p2[0]), (size_t) &f1_p2[0]);

	f1_y = f2(10);

	return;
}

/**
 * Driver program.
 * @param  {int}    argc
 * @param  {char *} argv
 * @return {int}
 */
int main(int argc, char * argv[]) {
	// count environ items
	int environ_count = 0;

	while (environ[environ_count]) {
		environ_count++;
	}

	//printf("argc: %p | %zu | %zd | %zx\n", &argc, (size_t) &argc, (size_t) &argc, (size_t) &argc);

	// record and display
	printf("My OS bit size: %lu\n", sizeof(void *) * 8);

	// argc, argv, and environ
	record("argc",          "func arg int",    '-', sizeof(argc),                   (size_t) &argc);
	record("argv",          "func arg char *", '-', sizeof(argv[0]),                (size_t) &argv);
	record("argv start",    "char * []",       '-', sizeof(argv[0]),                (size_t) &argv[0]);
	record("argv end",      "char * []",       '-', sizeof(argv[argc- 1]),          (size_t) &argv[argc - 1]);
	record("environ start", "char ** []",      '-', sizeof(environ[0]),             (size_t) &environ[0]);
	record("environ end",   "char ** []",      '-', sizeof(environ[environ_count]), (size_t) &environ[environ_count]);

	// functions
	record("main", "func", '-', sizeof(main), (size_t) main);
	record("f1",   "func", '-', sizeof(f1),   (size_t) f1);
	record("f2",   "func", '-', sizeof(f2),   (size_t) f2);

	// global vars
	record("global_x",        "global int",     'Y', sizeof(global_x),        (size_t) &global_x);
	record("global_y",        "global int",     'N', sizeof(global_y),        (size_t) &global_y);
	record("global_array1",   "global char []", 'Y', sizeof(global_array1),   (size_t) &global_array1);
	record("global_array2",   "global char []", 'N', sizeof(global_array2),   (size_t) &global_array2);
	record("global_pointer1", "global char *",  'Y', sizeof(global_pointer1), (size_t) &global_pointer1);
	record("global_pointer2", "global char *",  'N', sizeof(global_pointer2), (size_t) &global_pointer2);
	record("global_float",    "global float",   'Y', sizeof(global_float),    (size_t) &global_float);
	record("global_double",   "global double",  'N', sizeof(global_double),   (size_t) &global_double);

	// string literals
	record("\"Hello, world!\"", "string literal", '-', sizeof(global_array1[0]),   (size_t) &global_array1[0]);
	record("\"bye!\"",          "string literal", '-', sizeof(global_pointer1[0]), (size_t) &global_pointer1[0]);

	// run f1()
	f1(12, -5, 33.7, 'A', 1.896e-10, 100);

	sort_addresses();

	if (argc > 1 && strcmp(argv[1], "csv") == 0) {
		print_csv();
	} else {
		print_addresses();
	}

	exit(0);
}
