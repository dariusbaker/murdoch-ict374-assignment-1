## Brief/ask

Write a program that takes a list of command line arguments, each of which is the full path of a command (such as `/bin/ls`, `/bin/ps`, `/bin/date`, `/bin/who`, `/bin/uname` etc). Assume the number of such commands is N, your program would then create N direct child processes (ie, the parent of these child processes is the same original process), each of which executing one of the N commands.

You should make sure that these N commands are executed concurrently, not sequentially one after the other. The parent process should be waiting for each child process to terminate. When a child process terminates, the parent process should print one line on the standard output stating that the relevant command has completed successfully or not successfully (such as "Command /bin/who has completed successfully", or "Command /bin/who has not completed successfully"). Once all of its child processes have terminated, the parent process should print "All done, bye-bye!" before it itself terminates.

Note: do not use function `system` in this question.