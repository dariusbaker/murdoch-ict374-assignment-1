#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

typedef struct process {
	char * name;
	int    id;
} Process;

/**
 * Driver program.
 * @param  {int}    argc
 * @param  {char *} argv[]
 * @return {int}
 */
int main(int argc, char * argv[]) {
	int i;
	int status;
	pid_t pid;
	Process * processes[argc - 1];

  // exit if no command line arguments
	if (argc == 1) {
		printf("No command line arguments provided. Exiting program.\n");
		exit(0);
	}

	// skip 0 because it's filename
	for (i = 1; i < argc; i++) {
		pid = fork();

		// main process logic
		if (pid > 0) {
			int pi = i - 1;

			// we'll save the process ID and name so that we may get the name later
			processes[pi] = malloc(sizeof(Process));
			processes[pi]->name = argv[i];
			processes[pi]->id   = pid;
		}

		// child process logic
		if (pid == 0) {
			//printf("Process '%s' PID: %d | PPID: %d\n", argv[i], getpid(), getppid());
			execl(argv[i], argv[i], NULL);

			// this should only be reached if there was an error with 'execl'
			printf("Command %s has not completed successfully.\n", argv[i]);
			exit(1);
		}
	}

	// main process continues
	while ((pid = wait(&status)) > 0) {
		if (WIFEXITED(status)) {
			// check if the process exited with 0
			if (status == 0) {
				i = 0;

				// find command name that matches process ID
				while (processes[i] != NULL) {
					if (processes[i]->id == pid) {
						printf("Command %s has completed successfully.\n", processes[i]->name);
						break;
					}
					i++;
				}
			}
		}
	}

	printf("All done, bye-bye!\n");

	exit(0);
}
